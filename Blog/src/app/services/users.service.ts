import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class UsersService {
  
    private usuarioActual:number = null;
    
    private users:number;
    private post:any;
    private elpost:any;

    getPostCommentsURL:string = "http://192.168.101.61:8080/social/comment";
    setPostCommentsURL:string = "http://192.168.101.61:8080/social/comment";
    constructor(private http:Http) { }

    getUsersAPI(){

        console.log("llama a getUsers API");
      
         let header = new Headers({'Content-Type':'application/json'});
         let usersURL = "http://192.168.101.61:8080/social/users";
      
         return this.http.get(usersURL, {headers: header}).map(res =>{
           console.log(res.json());
           console.log("entro positivo REST getUsersAPI")
           this.users = res.json().results;
           console.log(this.users);
           
           return res.json();
         }, err => console.log("error: "+err.json()));
      }

    setUsuarioActual(id:number){
      this.usuarioActual = id;
      console.log("id user");
      console.log(this.usuarioActual);
    }

    getUsuarioActual(){
      return this.usuarioActual;
      
      
    }

      getPostAPI(){

        console.log("llama a getPostAPI");
      
         let header = new Headers({'Content-Type':'application/json'});
         let postURL = "http://192.168.101.61:8080/social/posts";
      
         return this.http.get(postURL, {headers: header}).map(res =>{
           console.log(res.json());
           console.log("entro positivo REST getPostAPI")
           this.post = res.json().results;
           
           console.log(this.post);
           
           return res.json();
         }, err => console.log("error: "+err.json()));
      }

      getElPost(id:number){
        return this.elpost[id];
      }


      getElPostAPI(id:number){

        console.log("llama a getElPost API");
      
         let header = new Headers({'Content-Type':'application/json'});
         let postURL = "http://192.168.101.61:8080/social/comment/"+id;
      
         return this.http.get(postURL, {headers: header}).map(res =>{
           console.log(res.json());
           console.log("entro positivo REST getElPostAPI")
           this.elpost = res.json().results;
           
           console.log(this.elpost);
           
           return res.json();
         }, err => console.log("error: "+err.json()));
      }

      postComentariosAPI(comentario:any){
        console.log("llama a postComentarioAPI");
        let header = new Headers ({'Content-Type':'application/json'})
        let comentariosURL = "http://192.168.101.61:8080/social/comment";
        let body = comentario;
        return this.http.post(comentariosURL, body, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST postComentariosAPI")
             return res.json();
           }, err => console.log("error: "+err.json()));
      }

      getPostComments(idPost:number,idUser:number,comment:string){
        console.log("llama a getPostComments API");
        let header = new Headers({'Content-Type':'application/json'});
        
        return this.http.get(this.getPostCommentsURL+"/"+idPost,{headers: header}).map(res =>{
          console.log(res.json());
          console.log("entro positivo REST getPostComments");
          this.elpost = res.json();
          return res.json();
        }, err => console.log("error: "+err.json()));

      }
      setPostComments(idpost:number,iduser:number,Comment:string){

        console.log("llama a setPostComments API");
      
         let header = new Headers({'Content-Type':'application/json'});
         
         let creoComentario = {
          idUser:  iduser,
          idPost:  idpost,
          comment:  Comment
          }

         return this.http.post(this.setPostCommentsURL,creoComentario, {headers: header})
             .map(res =>{
               console.log(res);
               console.log(res.json());
               console.log("entro positivo REST setPostComments")
               this.elpost = res.json();
               return res.json();
             }, err => console.log("error: "+err.json()));
      
      
        }
        
      

    ngOnInit() {
    }
  
}