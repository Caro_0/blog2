import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { UsersComponent } from './users/users.component';
import { appRouting } from './app.routes';
import { HttpModule } from '@angular/http';
import { UsersService } from './services/users.service';
import { PostComponent } from './post/post.component';
import { ElpostComponent } from './elpost/elpost.component'
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsersComponent,
    PostComponent,
    ElpostComponent,
    
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpModule,
    FormsModule
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
