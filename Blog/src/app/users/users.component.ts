import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  
  users:any;
  idusers:number;
  constructor(private usersService:UsersService, private router:Router) { }

  ngOnInit() {
    
    this.usersService.getUsersAPI().subscribe(data => {
      console.log(data)
      this.users = data.results;
      this.idusers = data.results.id;
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
  
      
      
    }

    
    
    
    
    verPost(idUser){
      this.usersService.setUsuarioActual(idUser);
      
      this.router.navigate(['/post']);  
    }
}
