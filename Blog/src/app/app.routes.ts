import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { PostComponent } from './post/post.component';
import { ElpostComponent } from './elpost/elpost.component';
const routes: Routes = [
    { path: 'users', component: UsersComponent },
    { path: 'navbar', component: NavbarComponent},
    { path: 'post', component: PostComponent},
    { path: 'elpost/:id', component: ElpostComponent},
    { path: '**', pathMatch:'full', redirectTo: 'users' }
];

export const appRouting = RouterModule.forRoot(routes);