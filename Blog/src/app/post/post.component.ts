import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  post:any;

  constructor(private activatedRoute:ActivatedRoute,private usersServices:UsersService, private router:Router) { }

  ngOnInit() {
    
    this.usersServices.getPostAPI().subscribe(data => {
      console.log("data recibida");
      console.log(data);
      this.post = data.results;
    },
       error => {
         console.log("fallo el call de la API");
         console.log("error");        
       });
    }

    

    verElPost(id:number){
      console.log('imprimo parameto: '+id);
      this.router.navigate(['/elpost',id]);
    }
}
