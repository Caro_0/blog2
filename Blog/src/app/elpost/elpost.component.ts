import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-elpost',
  templateUrl: './elpost.component.html',
  styleUrls: ['./elpost.component.css']
})
export class ElpostComponent implements OnInit {

  private newComment:string = " ";

 creoComentario = {
  idUser: null,
  idPost: null,
  comment: ''
}

  elpost:any;
  comentarios:any;

  constructor(private activatedRoute:ActivatedRoute, private usersServices:UsersService, private router:Router) {
    this.activatedRoute.params.subscribe(params => {
      this.creoComentario.idPost = params["id"];
        console.log("Parametro recibido: " + params["id"]);
  this.usersServices.getElPostAPI(params["id"]).subscribe(data => {
    console.log(data) //recibo en data el json (res.json)
    this.elpost = data.post;
    this.comentarios = data.results;
    console.log("comentarios:"+ this.comentarios);
    
    console.log("llegaron los datos");
    
  },
     error => {
       console.log("fallo el call de la API ");
     
       console.log(error)
     });
      });    
    }

guardar(forma:NgForm) {

  console.log("guardo los cambios");
  console.log("NgForm forma: ", forma);
  console.log("forma value: ", forma.value);
  
  this.creoComentario.idUser = this.usersServices.getUsuarioActual();
 /* this.usersServices.postComentariosAPI(this.creoComentario).subscribe(data => {
    console.log(data) //recibo en data el json (res.json)
    console.log(data.results);
    
  },
     error => {
       console.log("fallo el call de la API guardar");
     
       console.log(error)
     }); */

     
     this.usersServices.setPostComments(this.elpost.id,this.usersServices.getUsuarioActual(),this.creoComentario.comment)
    .subscribe(data => {
      console.log(data)
      console.log("llamo exitosamente a PostComment")
      this.activatedRoute.params.subscribe(params => {
        this.creoComentario.idPost = params["id"];
          console.log("Parametro recibido: " + params["id"]);
    this.usersServices.getElPostAPI(params["id"]).subscribe(data => {
      console.log(data) //recibo en data el json (res.json)
      this.elpost = data.post;
      this.comentarios = data.results;
      console.log("comentarios:"+ this.comentarios);
      
      console.log("llegaron los datos");
      
    },
       error => {
         console.log("fallo el call de la API ");
       
         console.log(error)
       });
        });  
    },
      error => {
        console.log("fallo el call de la API PostComment");
        
        console.log(error)
      });
      console.log("creoComentario ", this.creoComentario);

}


      ngOnInit() {}

      
      
}
